from urllib.parse import urlencode
import json

def call(client, path):
    return str(client.get(path).status)


def test_home(client):
    result = call(client, '/')
    assert result == "200 OK"
