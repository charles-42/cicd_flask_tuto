import pytest
@pytest.fixture
def client():
    from web import main
    main.app.config['TESTING'] = True
    return main.app.test_client()